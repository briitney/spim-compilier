%{
#include "Shared.h"
#include "Parser.h"

using namespace std;
int yyerror (const char * er);
extern int yylex ();
extern int yylineno;
extern char * yytext;
int string_count = 0;
int float_count = 0;
int cmp_count = 0;
int and_count = 0;
int or_count = 0;
int if_count = 0;
int while_count = 0;
int fp;

struct variable {
    char* name;
    char type;
    int offset;
};

// I know I'm wasting space by having both a float and a char* when I need one or the other
// The type and number are combined to make the label for the data
struct data {
    float f_val;
    char* s_val;
    char type;
    int index;
};

struct function {
    char* name;
    char ret_type;
    vector<variable>* parameters;
    int defnd;
};

vector<function> myfuncs;

// Anything being stored as a varriable needs to know its name, what type of varriable it is, and its position on the stack.
vector<variable> myvars;

vector<variable>* cur_varlist;

vector<variable>* cur_param = NULL;

vector<char>* cur_args = NULL;

char* cur_func_name;

char cur_func_call_type;

// Anything being put on the data segment just needs to know its type and value
vector<data> mydata;

%}
%union {
  int an_int;
  float a_float;
  char *a_string;
  char a_char;
  void *a_void;
}

%left '+' '-'
%left '%' '*' '/'
%nonassoc UNARY
%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%token <an_int> INT
%token <a_float> FLOAT
%token <a_string> STRING
%token <a_string> WORD
%token UNARY
%token RETURN
%token ECRIVEZ
%token PENDANT
%token IF
%token ELSE
%token <a_char> ENTIER
%token <a_char> REEL
%token <a_char> RIEN
%token EQUAL
%token NEQUAL
%token GTE
%token LTE
%token AND
%token OR

%%

PROGRAM : 
		{
			// Start the main method of the program
			add_myfuncs('v', (char*)"COMMENCEMENT", new vector<variable>, 0);
		}
	FUNCTION_DECLS
		{
			cout << "main:\n";
			cout << "\tmove $fp, $sp\n";
			cout << "\tsub $sp,$sp,8\n";
			cout << "\tjal _fnCOMMENCEMENT\n";
			cout << "\tli $v0,10\n";
			cout << "\tsyscall\n";
			print_data();
		}
FUNCTION_DECLS : FUNCTION_DECLS FUNCTION_DECL
	|
	;

FUNCTION_DECL : TYPE WORD '(' {cur_param = new vector<variable>;} PARAMETERS ')' FUNCTION_DECL_2

FUNCTION_DECL_2 : '{' 
		{
			fp = 0;
			cur_varlist = new vector<variable>;
			cur_func_name = $<a_string>-4;
			if(check_myfuncs($<a_char>-5, $<a_string>-4, cur_param) == -1) {
				yyerror("Function/prototype mismatch");
			} else if (check_myfuncs($<a_char>-5, $<a_string>-4, cur_param) == 0) {
				add_myfuncs($<a_char>-5, str_upr($<a_string>-4), cur_param, 1);
			}
			if ((get_myfuncs(str_upr($<a_string>-4)))->defnd == 1) {
				yyerror("Multiply declared function");
				return 0;
			}
			(get_myfuncs(str_upr($<a_string>-4)))->defnd = 1;
			cout << "_fn" << str_upr($<a_string>-4) << ":\n";
			cout << "\tsw $fp,8($sp)\n";
			cout << "\tsw $ra,4($sp)\n";
			cout << "\tla $fp,4($sp)\n";

		}
	STATEMENTS '}'
		{
			cout << "_ret" << str_upr($<a_string>-4) << ":\n";
			cout << "\tmove $sp,$fp\n";
			cout << "\tlw $fp,4($sp)\n";
			cout << "\tlw $ra,($sp)\n";
			cout << "\tadd $sp,$sp,8\n";
			cout << "\tjalr $ra\n";
		}
	| ';' 
		{
			if (get_myfuncs(str_upr($<a_string>-4)) == NULL) {
				add_myfuncs($<a_char>-5, str_upr($<a_string>-4), cur_param, 0);
			} else {
				yyerror("Function has already been declared");
			}
		}
	| error '}'
	;

PARAMETERS : PARAMETERS ',' PARAMETER
	| PARAMETER
	|
	;

PARAMETER : REEL WORD {
		push_to_vars($<a_char>1, $2, cur_param, 0);
	}
	| ENTIER WORD {
		push_to_vars($<a_char>1, $2, cur_param, 0);
	}
	;

INNER_STATEMENTS : INNER_STATEMENTS INNER_STATEMENT
	|
	;
	
INNER_STATEMENT : FUNCTION_CALL_STMT
	| VARIABLE_ASIN ';'
	| {yyerror("Internal variable declaration");} VARIABLE_DECL ';'
	| WHILE_LOOP
	| IF_STMT 
	| RETURN_STMT ';'
	| error '}'
	| error ';'
	;

STATEMENTS : STATEMENTS STATEMENT
	|
	;
	
STATEMENT : FUNCTION_CALL_STMT
	| VARIABLE_ASIN ';'
	| VARIABLE_DECL ';'
	| WHILE_LOOP
	| IF_STMT 
	| RETURN_STMT ';'
	| error '}'
	| error ';'
	;

RETURN_STMT : RETURN 
	{
		cout << "\tsub $sp,$sp,4\n";
	}
	NUMVAL
	{
		cout << "\tl.s $f0,($sp)\n";
		cout << "\ts.s $f0,8($fp)\n";
		cout << "\tb _ret" << str_upr(cur_func_name) << "\n";
	}

BRACE_STATEMENTS : INNER_STATEMENT 
	| '{' INNER_STATEMENTS '}'
	;

IF_STMT : IF_TEST BRACE_STATEMENTS ELSE 
		{
			cout << "_falseif" << $<an_int>1 << ":\n";
			cout << "\tbeq $t0,1,_falseelse" << $<an_int>1 << "\n";
		}
		BRACE_STATEMENTS
		{
			cout << "_falseelse" << $<an_int>1 << ":\n";
		}
	| IF_TEST BRACE_STATEMENTS 
		{	
			cout << "_falseif" << $<an_int>1 << ":\n";
		}
		%prec LOWER_THAN_ELSE
	;

IF_TEST : IF 
		{
			$<an_int>1 = if_count++;
			$<an_int>$ = $<an_int>1;
		}
		'(' SCBOOL ')'
		{
			cout << "\tbeq $t0,0,_falseif" << $<an_int>1 << "\n";
		}

WHILE_LOOP : PENDANT 
		{
			$<an_int>1 = while_count++;
			cout << "_begwhile" << $<an_int>1 << ":\n";
		}
		'(' EXPRESSION 
		{
			if ($<a_char>4 == 'f') yyerror("Cannot have break condition be a float");
			cout << "\tbeq $t0,0,_endwhile" << $<an_int>1 << "\n";
		}
		')' BRACE_STATEMENTS
		{
			cout << "\tb _begwhile" << $<an_int>1 << "\n";
			cout << "_endwhile" << $<an_int>1 << ":\n";
		}
	;

TYPE : ENTIER
	| REEL
	| RIEN
	;

VARIABLE_DECL : TYPE WORD_LIST

VARIABLE_ASIN : WORD '=' EXPRESSION
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if ($<a_char>3 == 'i') {
				cout << "\tlw $t0, ($sp)\n";
				intstuff ($<an_int>3, myvar);
			} else if ($<a_char>3 == 'f') {
				create_push_data($<a_char>3, &($<a_float>3));
				cout << "\tl.s $f0, ($sp)\n";
				floatstuff ($<a_float>3, myvar);
			} else if ($<a_char>3 == 'w') {
				variable* twovar = search_for_var($<a_string>3);
				if (twovar == NULL) {
					yyerror("Variable not declaired.");
					return 0;
				}
				if (twovar->type == 'i') {
					cout << "\tlw $t0,-" << twovar->offset << "($fp)\n";
					intstuff (0, myvar);
				}
				if (twovar->type == 'f') {
					cout << "\tl.s $f0,-" << twovar->offset << "($fp)\n";
					floatstuff (0, myvar);
				}
			}
			cout << "\tadd $sp,$sp,4\n";
		}

FUNCTION_CALL_STMT : FUNCTION_CALL ';'

FUNCTION_CALL : ECRIVEZ '(' PRINT_TYPE ')'
	| WORD '(' 
		{
			cur_args = new vector<char>();
			cout << "\tsub $sp,$sp,8\n";
			cur_func_call_type = get_myfuncs($<a_string>1)->ret_type;
		}
		VALUES ')'
		{
			$<a_void>2 = new vector<variable>();
			if (check_type_arity($1) == 1) {
				cout << "\tjal _fn" << str_upr($1)<< "\n" << endl;
			} else {
				yyerror("Function call arguments have wrong arity/types");
				return 0;
			}
			
		}
	;

VALUES : VALUES ',' VALUE
	| VALUE
	| 
	;

VALUE : NUMVAL {cur_args->push_back($<a_char>1);}
	;

PRINT_TYPE : STRING  
		{
			create_push_data('s', $1);
			cout << "\tli $v0,4\n";
			cout << "\tla $a0,str" << string_count << "\n";
			cout << "\tsyscall\n";
		}
	| EXPRESSION
		{
			if ($<a_char>1 == 'i') {
				cout << "\tli $v0,1\n";
				cout << "\tlw $a0,($sp)\n";
				cout << "\tsyscall\n";
			} else if ($<a_char>1 == 'f') {
				cout << "\tli $v0,2\n";
				cout << "\tl.s $f12,($sp)\n";
				cout << "\tsyscall\n";
			} else {
				if (cur_func_call_type == 'i') {
					cout << "\tli $v0,1\n";
					cout << "\tlw $a0,($sp)\n";
					cout << "\tsyscall\n";
				} else if (cur_func_call_type == 'f') {
					cout << "\tli $v0,2\n";
					cout << "\tl.s $f12,($sp)\n";
					cout << "\tsyscall\n";
				}
			}
		}
	;

WORD_LIST : WORD_LIST ',' WORD {
		push_to_vars($<a_char>0, $3, cur_varlist, 1);
	}
	| WORD {
		push_to_vars($<a_char>0, $1, cur_varlist, 1);
	}
	;

NUMVAL : INT
		{
			$<a_char>$ = 'i';
			cout << "\tli $t0," << $1 << "\n";
			cout << "\tsw $t0,($sp)\n";
		}
	| FLOAT 
		{
			$<a_char>$ = 'f';
			create_push_data('f', &($1));
			cout << "\tl.s $f0,fl"<< float_count <<"\n";
			cout << "\ts.s $f0,($sp)\n";
		}
	| WORD 
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if (myvar->type == 'i') {
				$<a_char>$ = 'i';
				cout << "\tlw $t0,-" << myvar->offset << "($fp)\n";
				cout << "\tsw $t0,($sp)\n";
			}
			if (myvar->type == 'f') {
				$<a_char>$ = 'f';
				cout << "\tl.s $f0,-" << myvar->offset << "($fp)\n";
				cout << "\ts.s $f0,($sp)\n";
			}
		}
	| FUNCTION_CALL {cerr << $<a_char>$ << endl;}
	;

EXPRESSION: EXPR
	| SCBOOL {$<a_char>$ = 'i';}
	;

SCBOOL: SCBOOL OR 
		{
			$<an_int>2 = or_count++;
			cout << "lw $t0,($sp)\n";
			cout << "beq $t0,1,_skipor" << $<an_int>2 << "\n";
		}
		COND
		{
			cout << "_skipor" << $<an_int>2 << ":\n";
		}
	| SCBOOL AND
		{
			$<an_int>2 = and_count++;
			cout << "lw $t0,($sp)\n";
			cout << "beq $t0,0,_skipand" << $<an_int>2 << "\n";
		}
		COND
		{
			cout << "_skipand" << $<an_int>2 << ":\n";
		}
	| '(' SCBOOL ')'
	| '!' '(' SCBOOL ')'
		{
			cout << "\tlw $t0,($sp)\n";
			cout << "\tseq $t0,$t0,0\n";
			cout << "\tsw $t0,($sp)\n";
		}
	| COND
	;

COND: EXPR '>' EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, ">");
		}
	| EXPR '<' EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "<");
		}
	| EXPR EQUAL EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "==");
		}
	| EXPR NEQUAL EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "!=");
		}
	| EXPR GTE EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, ">=");
		}
	| EXPR LTE EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "<=");
		}
	;

EXPR: EXPR '+' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "add");
		}
	| EXPR '-' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "sub");
		}
	| EXPR '*' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "times");
		}
	| EXPR '/' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "div");
		}
	| EXPR '%' EXPR
		{
			if ($<a_char>1 == 'f' || $<a_char>3 == 'f') {
				cout << "\tadd $sp,$sp,4\n";
				yyerror("Cannot do floating point modulo");
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\trem $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n"; }
		}
	| '(' EXPR ')' {$<a_char>$ = $<a_char>2;}
	| '-' EXPR %prec UNARY
		{
			if ($<a_char>2 == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tneg.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if ($<a_char>2 == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tneg $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
			$<a_char>$ = $<a_char>2;
		}
	| '+' EXPR %prec UNARY
		{
			if ($<a_char>2 == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tabs.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if ($<a_char>2 == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tabs $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
			$<a_char>$ = $<a_char>2;
		}
	| {cout << "\tsub $sp, $sp, 4\n";} NUMVAL {$<a_char>$ = $<a_char>2;}
	;

%%

int yyerror (const char *msg) {
	fprintf(stderr, "file: %s line %d: %s at '%s'\n", curfilename, yylineno, msg, yytext);
}

void print_data () {
	cout << "\t.data\n";
	for (auto it = mydata.begin(); it != mydata.end(); ++it) {
		if ((*it).type == 'f') {
			printf("fl%i:\t.float %f\n", (*it).index, (*it).f_val);
		} else if ((*it).type == 's') {
			cout << "str" << (*it).index << ":\t.asciiz " << (*it).s_val << "\n";
		}
	}
}

void push_to_vars (char type, char* name, void* list, int incr) {
	variable d;
	d.name = strdup(name);
	d.type = type;
	fp += 4*incr;
	d.offset = fp;
	cout << "\tsub $sp, $sp, 4\n";
	((vector<variable>*)list)->push_back(d);
}

variable* search_for_var (char* searchterm) {
	for (vector<variable>::iterator it = myvars.begin(); it != myvars.end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	for (vector<variable>::iterator it = cur_param->begin(); it != cur_param->end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	for (vector<variable>::iterator it = cur_varlist->begin(); it != cur_varlist->end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	return NULL;
}

void floatstuff (float myfloat, variable* myvar) {
	cout << "\ts.s $f0, ($sp)\n";
	if (myvar->type == 'i') {
		cout << "\tl.s $f0,($sp)\n";
		cout << "\tcvt.w.s $f0,$f0\n";
		cout << "\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else {
		cout << "\tlw $t0,($sp)\n";
		cout << "\tsw $t0,-" << myvar->offset << "($fp)\n";
	}
}

void intstuff (int myint, variable* myvar) {
	if (myvar->type == 'f') {
		cout << "\tsw $t0,($sp)\n";
		cout << "\tl.s $f0,($sp)\n";
		cout << "\tcvt.s.w $f0,$f0\n";
		cout << "\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else cout
		<< "\tsw $t0,-" << myvar->offset << "($fp)\n";
}

void create_push_data (char vt, void* value) {
	data d;
	d.type = vt;
	if (vt == 'f') {
		d.f_val = *((float*) value);
		d.index = ++float_count;
	} else if (vt == 's') {
		d.s_val = (char*)value;
		d.index = ++string_count;
	}
	mydata.push_back(d);
}

char print_expression (char var1_type, char var2_type, const char* operation) {
	if (var1_type == 'f' || var2_type == 'f') {
		cout <<"\tl.s $f0,($sp)\n";
		cout <<"\tl.s $f2,4($sp)\n";
		if (var1_type == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
		if (var2_type == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
		if (strcmp(operation, "add") == 0) {
			cout <<"\tadd.s $f0,$f0,$f2\n";
		} else if (strcmp(operation, "sub") == 0) {
			cout <<"\tsub.s $f0,$f2,$f0\n";
		} else if (strcmp(operation, "times") == 0) {
			cout <<"\tmul.s $f0,$f2,$f0\n";
		} else if (strcmp(operation, "div") == 0) {
			cout <<"\tdiv.s $f0,$f2,$f0\n";
		}
		cout <<"\ts.s $f0,4($sp)\n";
		cout <<"\tadd $sp,$sp,4\n";
		return 'f';
	} else {
		cout << "\tlw $t1,($sp)\n";
		cout << "\tlw $t0,4($sp)\n";
		if (strcmp(operation, "add") == 0) {
			cout << "\tadd $t0,$t0,$t1\n";
		} else if (strcmp(operation, "sub") == 0) {
			cout << "\tsub $t0,$t0,$t1\n";
		} else if (strcmp(operation, "times") == 0) {
			cout << "\tmul $t0,$t0,$t1\n";
		} else if (strcmp(operation, "div") == 0) {
			cout << "\tdiv $t0,$t0,$t1\n";
		}
		cout << "\tsw $t0,4($sp)\n";
		cout << "\tadd $sp,$sp,4\n";
		return 'i';
	}
}

void print_comp (char var1_type, char var2_type, const char* symb) {
	if (var1_type == 'f' || var2_type == 'f') {
		cout <<"\tl.s $f2,($sp)\n";
		cout <<"\tl.s $f0,4($sp)\n";
		if (var1_type == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
		if (var2_type == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
		if (strcmp(symb, ">") == 0) {
			cout <<"\tc.lt.s $f2,$f0\n";
		} else if (strcmp(symb, "<") == 0) {
			cout <<"\tc.lt.s $f0,$f2\n";
		} else if (strcmp(symb, ">=") == 0) {
			cout <<"\tc.le.s $f2,$f0\n";
		} else if (strcmp(symb, "<=") == 0) {
			cout <<"\tc.le.s $f0,$f2\n";
		} else if (strcmp(symb, "==") == 0) {
			cout <<"\tc.eq.s $f0,$f2\n";
		} else if (strcmp(symb, "!=") == 0) {
			cout <<"\tc.eq.s $f0,$f2\n";
		}
		cout << "\tbc1t _cmp" << cmp_count << "\n";
		if ((strcmp(symb, "!=") == 0)) {
			cout << "\tli $t0,1\n";
		} else {
			cout << "\tli $t0,0\n";
		}
		cout << "\tb _aftercmp" << cmp_count << "\n";
		cout << "_cmp" << cmp_count << ":\n";
		if ((strcmp(symb, "!=") == 0)) {
			cout << "\tli $t0,0\n";
		} else {
			cout << "\tli $t0,1\n";
		}
		cout << "_aftercmp" << cmp_count << ":\n";
		cout << "\nsw $t0,4($sp)\n";
		cout << "\nadd $sp,$sp,4\n";
		cmp_count++;
	} else {
		cout << "\tlw $t1,($sp)\n";
		cout << "\tlw $t0,4($sp)\n";
		if (strcmp(symb, ">") == 0) {
			cout << "\tsgt $t0,$t0,$t1\n";
		} else if (strcmp(symb, "<") == 0) {
			cout << "\tslt $t0,$t0,$t1\n";
		} else if (strcmp(symb, ">=") == 0) {
			cout << "\tsge $t0,$t0,$t1\n";
		} else if (strcmp(symb, "<=") == 0) {
			cout << "\tsle $t0,$t0,$t1\n";
		} else if (strcmp(symb, "==") == 0) {
			cout << "\tseq $t0,$t0,$t1\n";
		} else if (strcmp(symb, "!=") == 0) {
			cout << "\tsne $t0,$t0,$t1\n";
		}
		cout << "\tsw $t0,4($sp)\n";
		cout << "\tadd $sp,$sp,4\n";
	}
} 

int check_myfuncs(char ret_type, char* name, void* parameters) {
	vector<variable>* params = (vector<variable>*)parameters;
	for (vector<function>::iterator it = myfuncs.begin(); it != myfuncs.end(); ++it) {
		if (strcasecmp((*it).name, name) == 0) {
			if ((*it).ret_type == ret_type) {
				if ((*it).parameters->size() == params->size()) {
					for (vector<variable>::iterator jt = (*it).parameters->begin(), kt = params->begin(); jt != (*it).parameters->end(); ++jt, ++kt) {
						if (strcasecmp((*jt).name, (*kt).name) != 0 || (*jt).type != (*kt).type) {
							return -1;
						}
					}
					return 1;
				}
			}
			return -1;
		}
	}
	return 0;
}

function* get_myfuncs(char* name) {
	for (vector<function>::iterator it = myfuncs.begin(); it != myfuncs.end(); ++it) {
		if (strcasecmp((*it).name, name) == 0) {
			return &(*it);
		}
	}
	return NULL;
}

void print_cur_param() {
	cerr << "param: ";
	for (vector<variable>::iterator it = cur_param->begin(); it != cur_param->end(); ++it) {
		cerr << (*it).name << " ";
	}
	cerr << endl;
}

void print_myfuncs() {
	cerr << "funcs: ";
	for (vector<function>::iterator it = myfuncs.begin(); it != myfuncs.end(); ++it) {
		cerr << (*it).name << " ";
	}
	cerr << endl;
}

void add_myfuncs(char ret_type, char* name, void* parameters, int defnd) {
	vector<variable>* params = (vector<variable>*)parameters;
	function f;
	f.name = strdup(name);
	f.ret_type = ret_type;
	f.parameters = params;
	f.defnd = defnd;
	myfuncs.push_back(f);
}

char* str_upr(char* str) {
	for (int i = 0; str[i]; i++) {
		str[i] = toupper(str[i]);
	}
	return str;
}

int check_type_arity(char* func_name) {
	function* func = get_myfuncs(func_name);
	if (func == NULL) {
		yyerror("Function has not been declared");
		return 0;
	}
	if (func->parameters->size() == cur_args->size()) {
		vector<char>::iterator kt = cur_args->begin();
		for (vector<variable>::iterator jt = func->parameters->begin(); jt != func->parameters->end(); ++jt, ++kt) {
			if ((*jt).type != (*kt)) {
				return -1;
			}
		}
		return 1;
	}
	return -1;
}
