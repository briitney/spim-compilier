%{
#include "Parser.h"
#include "types.h"

using namespace std;

int yyerror (const char * er);
extern int yylex ();
extern int yylineno;
extern char * yytext;
int string_count = 0;
int float_count = 0;
int fp = 4;

struct variable {
    char* name;
    char type;
    int offset;
};

// I know I'm wasting space by having both a float and a char* when I need one or the other
// The type and number are combined to make the label for the data
struct data {
    float f_val;
    char* s_val;
    char type;
    int index;
};

// Anything being stored as a varriable needs to know its name, what type of varriable it is, and its position on the stack.
vector<variable> myvars;

// Anything being put on the data segment just needs to know its type and value
vector<data> mydata;

// Used to keep track of what type the union is using
// Probably a better way to do this, but this works fine for variable asignments
char valuetype;

%}
%union {
  int an_int;
  float a_float;
  char *a_string;
  char type;
}

%token <an_int> INT
%token <a_float> FLOAT
%token <a_string> STRING
%token <a_string> WORD
%token COMMENCEMENT
%token ECRIVEZ
%token <type> ENTIER
%token <type> REEL

%%

PROGRAM : 
	{
		// Start the main method of the program
		cout << "\t.text\n\t.globl main\n";
	}
	FUNCTION_DECLS
	{
		cout << "\tli $v0, 10\n\tsyscall\n";
		print_data();
	}

FUNCTION_DECLS : FUNCTION_DECLS FUNCTION_DECL
	|
	;

FUNCTION_DECL : VALUE WORD '(' ')' '{' STATEMENTS '}'
	| VALUE COMMENCEMENT 
		{
			cout << "main:\n";
			// Initialize the frame pointer to the stack pointer
			cout << "\tmove $fp, $sp\n";
		}
		'(' ')' '{' STATEMENTS '}' 
	;

STATEMENTS : STATEMENTS STATEMENT ';'
	|
	;
	
STATEMENT : FUNCTION_CALL
	| VARIABLE_ASIN
	| VARIABLE_DECL
	| error
	;

TYPE : ENTIER
	| REEL
	;

VARIABLE_DECL : TYPE WORD_LIST

VARIABLE_ASIN : WORD '=' VALUE
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			cout << "\tsub $sp,$sp,4\n";
			if (valuetype == 'i') {
				cout << "\tli $t0," << $<an_int>3 << "\n";
				intstuff ($<an_int>3, myvar);
			} else if (valuetype == 'f') {
				create_push_data(valuetype, &($<a_float>3));
				cout << "\tl.s $f0, " << "fl" << float_count << "\n";
				floatstuff ($<a_float>3, myvar);
			} else if (valuetype == 'w') {
				variable* twovar = search_for_var($<a_string>3);
				if (twovar->type == 'i') {
					cout << "\tlw $t0,-" << twovar->offset << "($fp)\n";
					intstuff (0, myvar);
				}
				if (twovar->type == 'f') {
					cout << "\tl.s $f0,-" << twovar->offset << "($fp)\n";
					floatstuff (0, myvar);
				}
			}
			cout << "\tadd $sp,$sp,4\n";
		}

FUNCTION_CALL : ECRIVEZ '(' PRINT_TYPE ')'
	| ECRIVEZ '(' WORD ')'
		{
			variable* myvar = search_for_var($3);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			cout << "\tsub $sp,$sp,4\n";
			if (myvar->type == 'i') {
				cout << "\tlw $t0,-" << myvar->offset << "($fp)\n";
				cout << "\tsw $t0,($sp)\n";
				cout << "\tli $v0,1\n";
				cout << "\tlw $a0,($sp)\n";
			} else if (myvar->type == 'f') {
				cout << "\tl.s $f0,-" << myvar->offset << "($fp)\n";
				cout << "\ts.s $f0,($sp)\n";
				cout << "\tli $v0,2\n";
				cout << "\tl.s $f12,($sp)\n";
			}
			cout << "\tadd $sp,$sp,4\n";
			cout << "\tsyscall\n";
		}
	| WORD '(' VALUES ')'
	;

PRINT_TYPE : 
	| INT 
		{
			cout << "\tli $v0, 1\n";
			cout << "\tla $a0, " << $1 << "\n";
			cout << "\tsyscall\n";
		}
	| FLOAT  
		{
			create_push_data('f', &($1));
			cout << "\tli $v0, 2\n";
			cout << "\tl.s $f12, fl" << float_count << "\n";
			cout << "\tsyscall\n";
		}
	| STRING  
		{
			create_push_data('s', $1);
			cout << "\tli $v0, 4\n";
			cout << "\tla $a0, str" << string_count << "\n";
			cout << "\tsyscall\n";
		}
	;

WORD_LIST : WORD_LIST ',' WORD {
		push_to_myvars($<type>0, $3);
	}
	| WORD {
		push_to_myvars($<type>0, $1);
	}
	;

VALUES : VALUES ',' VALUE
	| VALUE
	;

VALUE : INT {valuetype = 'i';}
	| FLOAT {valuetype = 'f';}
	| STRING {valuetype = 's';}
	| WORD {valuetype = 'w';}
	;

%%

int yyerror (const char *msg) {
	fprintf(stderr, "line %d: %s at '%s'\n", yylineno, msg, yytext);
}

void print_data () {
	cout << "\t.data\n";
	for (auto it = mydata.begin(); it != mydata.end(); ++it) {
		if ((*it).type == 'f') {
			cout << "fl" << (*it).index << ":\t.float " << (*it).f_val << "\n";
		} else if ((*it).type == 's') {
			cout << "str" << (*it).index << ":\t.asciiz " << (*it).s_val << "\n";
		}
	}
}

void push_to_myvars (char type, char* name) {
	variable d;
	d.name = strdup(name);
	d.type = type;
	d.offset = fp;
	fp += 4;
	cout << "\tsub $sp, $sp, 4\n";
	myvars.push_back(d);
}

variable* search_for_var (char* searchterm) {
	for (vector<variable>::iterator it = myvars.begin(); it != myvars.end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	return NULL;
}

void floatstuff (float myfloat, variable* myvar) {
	if (myfloat < 0) {
		cout << "\tneg.s $f0, $f0\n";
		myfloat = -myfloat;
	}
	cout << "\ts.s $f0, ($sp)\n";
	if (myvar->type == 'i') {
		cout << "\tl.s $f0,($sp)\n\tcvt.w.s $f0,$f0\n\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else {
		cout << "\tlw $t0,($sp)\n\tsw $t0,-" << myvar->offset << "($fp)\n";
	}
}

void intstuff (int myint, variable* myvar) {
	if (myint < 0) {
		cout << "\tneg $t0, $t0\n";
	}
	if (myvar->type == 'f') {
		cout << "\tsw $t0,($sp)\n\tl.s $f0,($sp)\n\tcvt.s.w $f0,$f0\n\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else cout << "\tsw $t0,-" << myvar->offset << "($fp)\n";

}

void create_push_data (char vt, void* value) {
	data d;
	d.type = vt;
	if (vt == 'f') {
		d.f_val = *((float*) value);
		d.index = ++float_count;
	} else if (vt == 's') {
		d.s_val = (char*)value;
		d.index = ++string_count;
	}
	mydata.push_back(d);
}
