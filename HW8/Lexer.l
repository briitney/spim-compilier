%{
#include "Shared.h"
#include "Lexer.h"
#include "Parser.tab.h"

forward_list<char*> compiler_vars;
int comp_if_count = 0;
int skip_test = 0;
char* undecl_val;

/*Structure from O'Reily book*/
struct bufstack {
	struct bufstack *prev; /* previous entry */
	YY_BUFFER_STATE bs; /* saved buffer */
	int lineno; /* saved line number */
	char *filename; /* name of this file */
	FILE *f; /* current file */
} *curbs = 0;
char* curfilename;

%}

int [0-9]+
float [0-9]+(\.[0-9]+)?((e|E)(\-|\+)?[0-9]+)?
word [A-Za-z]([A-Za-z]|[0-9])*
string \"([^\n\"])*\"
beginning_comment \/\/.*
wrap_comment \/\*((\*(\**|([^\*/][^\*]*\*))*)|([^\*]([^\*]*|(\*+[^\*/]))*\**))\*\/
delimiter [\ \t\r\n]?

%option yylineno
%option noyywrap
%x COMP_DECL
%x COMP_UNDECL
%x COMP_IF
%x COMP_IFN
%x SKIP
%x ERROR
%x IFILE
%%

	/* Compilier variable things */

#(?i:inclusez)[\t\ ]*\" {BEGIN IFILE;}

<IFILE>[^ \t\n\"]+ {
		{ int c;
			while((c = yyinput()) && c != '\n') ;
		}
		if(!newfile(yytext))
			yyterminate(); /* no such file */
		BEGIN INITIAL;
	 }

<<EOF>> { 
		if(!popfile()) {
			if (comp_if_count > 0) cerr << "Unmatched compilier conditional" << endl;
			yyterminate();
		}
	}

#(?i:definissez)[\t\ ]* {BEGIN COMP_DECL;}

<COMP_DECL>{word} {compiler_vars.push_front(strdup(yytext)); BEGIN INITIAL;}

#(?i:undef)[\t\ ]* {BEGIN COMP_UNDECL;}

<COMP_UNDECL>{word} {undecl_val = strdup(yytext); compiler_vars.remove_if(comp_equal_to); BEGIN INITIAL;}

#(?i:sidef)[\t\ ]* {BEGIN COMP_IF; comp_if_count++;}

<COMP_IF>{word} {
		if (in_list(compiler_vars, yytext)) {
			BEGIN INITIAL;
		} else {
			skip_test = comp_if_count;
			BEGIN SKIP;
		}
	}

#(?i:sipdef)[\t\ ]* {BEGIN COMP_IFN; comp_if_count++;}

<COMP_IFN>{word} {
		if (!in_list(compiler_vars, yytext)) {
			BEGIN INITIAL;
		} else {
			skip_test = comp_if_count;
			BEGIN SKIP;
		}
	}

#(?i:sinon) {skip_test = comp_if_count; BEGIN SKIP;}

<SKIP>#(?i:sidef) {comp_if_count++;}

<SKIP>#(?i:fin) {
		comp_if_count--; 
		if(comp_if_count == 0) {
			BEGIN INITIAL;
		}
	}

<SKIP>#(?i:sinon) {
		if(comp_if_count == skip_test) {
			BEGIN INITIAL;
		}
	}

<SKIP>. {}

#(?i:fin) {comp_if_count--; if(comp_if_count < 0) cerr << "file: " << curfilename << " line: " << yylineno << ": unmatched #fin." << endl;}

	/* keywords */

(?i:commencement) {return COMMENCEMENT;}

(?i:ecrivez) {return ECRIVEZ;}

(?i:entier) {yylval.a_char = 'i'; return ENTIER;}

(?i:reel) {yylval.a_char = 'f'; return REEL;}

(?i:pendant) {return PENDANT;}

(?i:si) {return IF;}

(?i:sinon) {return ELSE;}

(==) {return EQUAL;}

(!=) {return NEQUAL;}

(>=) {return GTE;}

(<=) {return LTE;}

(&&) {return AND;}

(\|\|) {return OR;}

	/* values */

(\r\n|\n\r|\r|\n) {}

{int}	{yylval.an_int = atoi(yytext); return INT;}

{float}	{yylval.a_float = atof(yytext); return FLOAT;}

{word}	{yylval.a_string = strdup(yytext); return WORD;}

{string}	{yylval.a_string = strdup(yytext); return STRING;}

{wrap_comment}|{beginning_comment}	{}

[!(){};=,\+\-\*\/%<>]	{return yytext[0];}

[\ \t]	{}

. {BEGIN ERROR;}

<ERROR>./(\r\n|\n\r|\r|\n|;|\}) {cerr << "file: " << curfilename << " line: " << yylineno << ": parse error" << endl; BEGIN INITIAL;}

<ERROR>. {}

%%

int main (int argc, char* argv[]) {
	if(argc > 1) {
		newfile(argv[1]);
		if(argc > 2) {
			freopen(argv[2],"w",stdout);
		}
	}
	yyparse();
}

int newfile(char *fn) {
	FILE *f = fopen(fn, "r");
	struct bufstack *bs = new bufstack();
	if(!f) { perror(fn); return 0; }
	if(curbs)curbs->lineno = yylineno;
	bs->prev = curbs;
	bs->bs = yy_create_buffer(f, YY_BUF_SIZE);
	bs->f = f;
	bs->filename = fn;
	yy_switch_to_buffer(bs->bs);
	curbs = bs;
	yylineno = 1;
	curfilename = fn;
	return 1;
}

int popfile(void) {
	struct bufstack *bs = curbs;
	struct bufstack *prevbs;
	if(!bs) return 0;
	fclose(bs->f);
	yy_delete_buffer(bs->bs);
	prevbs = bs->prev;
	free(bs);
	if(!prevbs) return 0;
	yy_switch_to_buffer(prevbs->bs);
	curbs = prevbs;
	yylineno = curbs->lineno;
	curfilename = curbs->filename;
	return 1;
}

int in_list(forward_list<char*> list, char* test) {
	for (auto it = list.begin(); it != list.end(); ++it) {
		if (strcasecmp((*it), test) == 0) return 1;
	}
	return 0;
}

void cerr_comp_vars() {
	cerr << "\tlist: ";
	for (auto it = compiler_vars.begin(); it != compiler_vars.end(); ++it) {
		cerr << (*it) << " ";
	}
	cerr << endl;

}
bool comp_equal_to (const char* value) {
	if (strcasecmp(value, undecl_val) == 0) return true;
	return false;
}
