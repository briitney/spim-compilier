_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
# word list here
	sub $sp, $sp, 4
# word list here
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	l.s $f0,fl1
	s.s $f0,($sp)
	l.s $f0, ($sp)
	s.s $f0, ($sp)
	lw $t0,($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,($sp)
	l.s $f0,($sp)
	cvt.s.w $f0,$f0
	s.s $f0,-8($fp)
	add $sp,$sp,4
# word list here
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-12($fp)
	add $sp,$sp,4
_begwhile0:
	sub $sp, $sp, 4
	l.s $f0,-8($fp)
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,1000
	sw $t0,($sp)
	l.s $f2,($sp)
	l.s $f0,4($sp)
	cvt.s.w $f2,$f2
	c.lt.s $f0,$f2
	bc1t _cmp0
	li $t0,0
	b _aftercmp0
_cmp0:
	li $t0,1
_aftercmp0:

sw $t0,4($sp)

add $sp,$sp,4
	beq $t0,0,_endwhile0
	sub $sp, $sp, 4
	l.s $f0,-8($fp)
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,4
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	div.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	lw $t0,-12($fp)
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	mul.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	l.s $f0,($sp)
	l.s $f2,4($sp)
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	l.s $f0, ($sp)
	s.s $f0, ($sp)
	lw $t0,($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	lw $t0,-12($fp)
	sw $t0,($sp)
	lw $t0,($sp)
	neg $t0,$t0
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-12($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	l.s $f0, ($sp)
	s.s $f0, ($sp)
	lw $t0,($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	l.s $f0,-8($fp)
	s.s $f0,($sp)
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str1
	syscall
	b _begwhile0
_endwhile0:
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
fl1:	.float 1.000000
fl2:	.float -262527352832.000000
fl3:	.float -0.000000
fl4:	.float -262539935744.000000
str1:	.asciiz "\n"
