_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	l.s $f0,fl1
	s.s $f0,($sp)
	l.s $f0,($sp)
	abs.s $f0, $f0
	s.s $f0, ($sp)
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str1
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl2
	s.s $f0,($sp)
	l.s $f0,($sp)
	neg.s $f0, $f0
	s.s $f0, ($sp)
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str2
	syscall
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
fl1:	.float 17.000000
str1:	.asciiz "\n"
fl2:	.float 17.000000
str2:	.asciiz "\n"
