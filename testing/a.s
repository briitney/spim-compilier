_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	mul $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	l.s $f0,fl1
	s.s $f0,($sp)
	l.s $f0, ($sp)
	s.s $f0, ($sp)
	lw $t0,($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	li $v0,4
	la $a0,str1
	syscall
	sub $sp, $sp, 4
	li $t0,7
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str2
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl3
	s.s $f0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl4
	s.s $f0,($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str3
	syscall
	li $v0,4
	la $a0,str4
	syscall
	sub $sp, $sp, 4
	li $t0,7
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str5
	syscall
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str6
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl5
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	div.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str7
	syscall
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl6
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	div.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str8
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl7
	s.s $f0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl8
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	div.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str9
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	div.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str10
	syscall
	li $v0,4
	la $a0,str11
	syscall
	li $v0,4
	la $a0,str12
	syscall
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	mul $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str13
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl9
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	mul.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str14
	syscall
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl10
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	mul.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str15
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl11
	s.s $f0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl12
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	mul.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str16
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	mul.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str17
	syscall
	li $v0,4
	la $a0,str18
	syscall
	li $v0,4
	la $a0,str19
	syscall
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sub $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str20
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl13
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	sub.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str21
	syscall
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl14
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	sub.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str22
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl15
	s.s $f0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl16
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	sub.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str23
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	sub.s $f0,$f2,$f0
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str24
	syscall
	li $v0,4
	la $a0,str25
	syscall
	li $v0,4
	la $a0,str26
	syscall
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str27
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl17
	s.s $f0,($sp)
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f0,$f0
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str28
	syscall
	sub $sp, $sp, 4
	li $t0,3
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl18
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str29
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl19
	s.s $f0,($sp)
	sub $sp, $sp, 4
	l.s $f0,fl20
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str30
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	l.s $f0,-4($fp)
	s.s $f0,($sp)
	l.s $f0,($sp)
	l.s $f2,4($sp)
	cvt.s.w $f2,$f2
	add.s $f0,$f0,$f2
	s.s $f0,4($sp)
	add $sp,$sp,4
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str31
	syscall
	li $v0,4
	la $a0,str32
	syscall
	li $v0,4
	la $a0,str33
	syscall
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
fl1:	.float 2.300000
fl2:	.float -15594864771072.000000
str1:	.asciiz "Modulo: 7%2, 3.2%3.4\n"
str2:	.asciiz " "
fl3:	.float 3.200000
fl4:	.float 3.400000
str3:	.asciiz "\n"
str4:	.asciiz "Division: 7/2, 2/3, 2.2/3, 3/2.2, 3.3/2.2, a/b\n"
str5:	.asciiz " "
str6:	.asciiz " "
fl5:	.float 2.200000
str7:	.asciiz " "
fl6:	.float 2.200000
str8:	.asciiz " "
fl7:	.float 3.300000
fl8:	.float 2.200000
str9:	.asciiz " "
str10:	.asciiz " "
str11:	.asciiz "\n"
str12:	.asciiz "Multiplication: 2*3, 2.2*3, 3*2.2, 3.3*2.2, a*b\n"
str13:	.asciiz " "
fl9:	.float 2.200000
str14:	.asciiz " "
fl10:	.float 2.200000
str15:	.asciiz " "
fl11:	.float 3.300000
fl12:	.float 2.200000
str16:	.asciiz " "
str17:	.asciiz " "
str18:	.asciiz "\n"
str19:	.asciiz "Subtraction: 2-3, 2.2-3, 3-2.2, 3.3-2.2, a-b\n"
str20:	.asciiz " "
fl13:	.float 2.200000
str21:	.asciiz " "
fl14:	.float 2.200000
str22:	.asciiz " "
fl15:	.float 3.300000
fl16:	.float 2.200000
str23:	.asciiz " "
str24:	.asciiz " "
str25:	.asciiz "\n"
str26:	.asciiz "Addition: 2+3, 2.2+3, 3+2.2, 3.3+2.2, a+b\n"
str27:	.asciiz " "
fl17:	.float 2.200000
str28:	.asciiz " "
fl18:	.float 2.200000
str29:	.asciiz " "
fl19:	.float 3.300000
fl20:	.float 2.200000
str30:	.asciiz " "
str31:	.asciiz " "
str32:	.asciiz "\n"
str33:	.asciiz "I am done now!\n"
