_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	li $v0,4
	la $a0,str1
	syscall
	li $v0,4
	la $a0,str2
	syscall
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str3
	syscall
	li $v0,4
	la $a0,str4
	syscall
	sub $sp, $sp, 4
	l.s $f0,fl1
	s.s $f0,($sp)
	li $v0,2
	l.s $f12,($sp)
	syscall
	li $v0,4
	la $a0,str5
	syscall
	li $v0,4
	la $a0,str6
	syscall
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
str1:	.asciiz "Hi there!\n"
str2:	.asciiz "I am going to print 2:  "
str3:	.asciiz "\n"
str4:	.asciiz "Now I am going to print 5.8:  "
fl1:	.float 5.800000
str5:	.asciiz "\n"
str6:	.asciiz "I am done now!\n"
