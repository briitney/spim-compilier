	sub $sp, $sp, 4
_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,5
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str1
	syscall
	sub $sp,$sp,8
	lw $t0,-4($fp)
	sw $t0,($sp)
	jal _fnTEST

	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str2
	syscall
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
	sub $sp, $sp, 4
_fnTEST:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str3
	syscall
_retTEST:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
str1:	.asciiz "\n"
str2:	.asciiz "\n"
str3:	.asciiz "\n"
